FactoryBot.define do
  factory :user do
    email { |i| "user@example.com#{i}" }
    password { 'passwordd' }
  end
end
