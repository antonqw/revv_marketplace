FactoryBot.define do
  factory :payment_transaction do
    product { nil }
    user { nil }
    amount { 9.99 }
  end
end
