FactoryBot.define do
  factory :product do
    title  { Faker::Name.name }
    description { 'Description of product' }
    price { 100 }
  end
end
