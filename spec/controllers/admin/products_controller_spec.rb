require 'rails_helper'

RSpec.describe Admin::ProductsController, type: :controller do
  describe 'application_controller#authenticate_seller!' do
    context 'user is not login' do
      it 'redirects to sign in page' do
        get :index
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'user is login in' do
      login_user

      it 'redirects to root page' do
        get :index
        expect(response).to redirect_to(root_path)
      end
    end

    context 'seller is login in' do
      login_seller

      it 'redirects to root page' do
        get :index
        expect(response).to have_http_status(200)
      end
    end
  end
end