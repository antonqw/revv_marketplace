require 'rails_helper'

RSpec.describe OmniauthCallbacksController, type: :controller do
  login_user

  describe 'get #stripe_connect' do

    it 'returns HTTP status 200' do
      request.env['omniauth.auth'] = { uid: '432421' }
      get :stripe_connect
      expect(response.status).to redirect_to(admin_root_path)
    end
  end
end
