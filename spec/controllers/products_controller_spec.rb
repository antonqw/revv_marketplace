require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  login_user

  describe 'get #index' do
    let!(:products) { create_list(:product, 2) }

    it 'returns HTTP status 200' do
      get :index
      expect(response.status).to eq 200
    end

    it 'returns wine sets list' do
      get :index
      expect(assigns(:products).count).to eq(2)
    end
  end

  describe 'get #show' do
    let(:product) { create(:product) }

    it 'returns HTTP status 200' do
      get :show, params: { id: product.id }
      expect(response.status).to eq 200
    end
  end
end
