require 'rails_helper'

RSpec.describe PaymentTransactionsController, type: :controller do
  login_user

  let(:product) { create(:product) }
  let(:payment_transaction) do
    create(:payment_transaction, user_id: subject.current_user.id, product_id: product.id)
  end

  describe 'post #create' do
    before do
      allow_any_instance_of(PaymentService).to receive(:call)
        .and_return(OpenStruct.new(result: payment_transaction))
    end

    it 'redirects to thanks page' do
      post :create, params: { payment_transaction: payment_transaction.attributes }
      expect(response).to redirect_to(payment_transaction_path(payment_transaction))
    end
  end

  describe 'get #show' do
    it 'redirects to thanks page' do
      get :show, params: { id: payment_transaction.id }
      expect(response.status).to eq 200
    end
  end
end
