require 'rails_helper'

RSpec.describe MoneyHelper, type: :helper do
  describe '#to_cents(amount)' do
    let(:amount) { 1234.43 }

    it 'converts dollars to cents' do
      expect(helper.to_cents(amount)).to eq 123_443
    end
  end
end
