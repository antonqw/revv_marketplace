require 'rails_helper'

describe PaymentService do
  it 'is available as described_class' do
    expect(described_class).to eq(PaymentService)
  end

  let(:user) { create(:user, password: 'password', stripe_user_id: '43223df') }
  let(:product) { create(:product) }
  let(:stripe_token) { 'fdsfds' }
  let(:valid_params) { [{ product_id: product.id, user_id: user.id, amount: 100.23 }, 'token'] }

  describe '#call' do
    before { allow_any_instance_of(described_class).to receive(:create_charge).and_return(true) }

    it 'success creates payment transaction' do
      command = described_class.call(*valid_params)
      expect(command.result.class).to eq(PaymentTransaction)
    end

    it 'delivers thanks email to user' do
      described_class.call(*valid_params)
      ActionMailer::Base.deliveries.last.to.should == user.email
      ActionMailer::Base.deliveries.last.subject == 'Thank you for your payment!'
    end
  end

  describe '-fee' do
    let(:amount) { 100.23 }
    before do
      allow_any_instance_of(described_class).to receive(:payment_transaction)
        .and_return(OpenStruct.new(amount: amount))
    end

    it 'calculates fee from amount' do
      expect(described_class.new(*valid_params).send(:fee)).to eq 1002
    end
  end
end
