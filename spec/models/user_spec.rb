require 'rails_helper'

RSpec.describe Product, type: :model do
  context 'associations' do
    it { should have_many(:payment_transactions).dependent(:destroy) }
  end
end
