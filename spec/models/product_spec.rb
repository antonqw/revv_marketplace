require 'rails_helper'

RSpec.describe Product, type: :model do
  context 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:price) }
    it { should validate_uniqueness_of(:title) }
  end

  context 'associations' do
    it { should have_many(:payment_transactions).dependent(:destroy) }
  end
end
