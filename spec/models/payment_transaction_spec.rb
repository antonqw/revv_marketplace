require 'rails_helper'

RSpec.describe PaymentTransaction, type: :model do
  context 'validations' do
    it { should validate_presence_of(:product) }
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:amount) }
  end

  context 'associations' do
    it { should belong_to(:product) }
    it { should belong_to(:user) }
  end
end
