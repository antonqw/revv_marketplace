module ControllerHelper
  def login_user
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = create(:user, password: 'password')
      sign_in user
    end
  end

  def login_seller
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = create(:user, password: 'password', stripe_user_id: '4324')
      sign_in user
    end
  end
end
