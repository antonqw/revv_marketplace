Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  devise_for :users, controllers: { omniauth_callbacks: "omniauth_callbacks" }

  devise_scope :user do
    authenticated :user do
      root 'products#index'
    end
    unauthenticated do
      root 'devise/sessions#new'
    end
  end

  resources :products, only: %i[index show]
  resources :payment_transactions, only: %i[create show]
end
