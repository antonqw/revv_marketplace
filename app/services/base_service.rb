class BaseService
  class << self
    def call(*args)
      new(*args).call
    end
  end

  def build_success_object(result='')
    OpenStruct.new(success?: true, result: result)
  end

  def build_error_object(error)
    OpenStruct.new(success?: false, error: error)
  end
end

class ServiceError < StandardError
  attr_reader :result

  def initialize(result)
    @result = result
  end
end
