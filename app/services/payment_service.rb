class PaymentService < BaseService
  include MoneyHelper

  FEE_PERCENT = 10
  CURRENCY = 'USD'.freeze

  def initialize(payment_params, stripe_token)
    @payment_params = payment_params
    @stripe_token = stripe_token
  end

  def call
    ActiveRecord::Base.transaction do
      create_payment_transaction
      create_charge
      send_mail
    end
    build_success_object(payment_transaction)
  rescue ServiceError => error
    error.result
  end

  private

  attr_reader :stripe_token, :payment_params, :payment_transaction

  def create_payment_transaction
    @payment_transaction = PaymentTransaction.create(payment_params)
  end

  def create_charge
    Stripe::Charge.create({ amount: to_cents(payment_transaction.amount),
                            currency: CURRENCY,
                            source: stripe_token,
                            application_fee: fee }, stripe_account: User.seller.stripe_user_id)
  rescue Stripe::CardError => e
    raise ServiceError, build_error_object(e.message)
  end

  def fee
    to_cents(payment_transaction.amount * FEE_PERCENT / 100)
  end

  def send_mail
    PaymentTransactionMailer.paid(payment_transaction.user, payment_transaction).deliver_now
  end
end
