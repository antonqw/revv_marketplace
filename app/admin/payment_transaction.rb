ActiveAdmin.register PaymentTransaction do
  actions :index, :show

  index do
    selectable_column
    id_column
    column :product
    column :user
    column :amount do |payment_transaction|
      number_to_currency(payment_transaction.amount)
    end
    column :updated_at
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :product
      row :user
      row :amount do |payment_transaction|
        number_to_currency(payment_transaction.amount)
      end
      row :created_at
      row :updated_at
    end
  end
end