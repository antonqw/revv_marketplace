ActiveAdmin.register Product do
  permit_params :title, :description, :price

  index do
    selectable_column
    id_column
    column :title
    column :description
    column :price do |product|
      number_to_currency(product.price)
    end
    column :updated_at
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :title
      row :description
      row :price do |product|
        number_to_currency(product.price)
      end
      row :created_at
      row :updated_at
    end
  end
end