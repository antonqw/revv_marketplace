#= require jquery
#= require rails-ujs
#= require activestorage
#= require bootstrap-sprockets
#= require turbolinks
#= require ./flash_messages
#= require_tree .
