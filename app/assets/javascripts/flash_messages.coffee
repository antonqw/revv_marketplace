$ ->
  $(document).ready ->
    window.show_flash_messages()

  window.show_flash_messages = ->
    setTimeout (->
      $('.popup_status').addClass('show').delay(2000).queue (next) ->
        $(this).removeClass 'show'
    ), 200
