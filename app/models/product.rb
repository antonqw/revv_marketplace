class Product < ApplicationRecord
  has_many :payment_transactions, dependent: :destroy

  validates :title, :description, :price, presence: true
  validates :title, uniqueness: true
end
