class PaymentTransaction < ApplicationRecord
  belongs_to :product
  belongs_to :user

  validates :amount, :user, :product, presence: true
end
