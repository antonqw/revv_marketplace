class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:stripe_connect]

  has_many :payment_transactions, dependent: :destroy

  validates :stripe_user_id,
            exclusion: { in: [Rails.application.credentials.stripe[:owner_stripe_user_id]] }

  def self.seller
    User.where.not(stripe_user_id: nil).take
  end

  def can_become_seller?
    !stripe_user_id && User.where.not(stripe_user_id: nil).empty?
  end

  def seller?
    stripe_user_id.present?
  end
end
