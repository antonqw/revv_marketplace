class PaymentTransactionMailer < ApplicationMailer
  def paid(user, payment_transaction)
    @payment_transaction = payment_transaction
    mail(to: user.email, subject: 'Thank you for your payment!')
  end
end
