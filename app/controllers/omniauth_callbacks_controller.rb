class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def stripe_connect
    auth_data = request.env['omniauth.auth']
    if current_user.update(stripe_user_id: auth_data['uid'])
      flash[:notice] = 'Stripe Account connected'
      redirect_to admin_root_path
    else
      flash[:alert] = 'Sorry, but you can not become seller'
      redirect_to root_path
    end
  end

  def failure
    flash[:alert] = params['error_description']
    redirect_to root_path
  end
end
