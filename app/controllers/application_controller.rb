class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  def authenticate_seller!
    redirect_to root_path unless current_user.seller?
  end
end
