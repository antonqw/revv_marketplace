class PaymentTransactionsController < ApplicationController
  def create
    @payment_transaction = PaymentService.call(payment_transaction_params,
                                               params[:stripeToken]).result
    redirect_to payment_transaction_path(@payment_transaction)
  end

  def show
    @payment_transaction = current_user.payment_transactions.find(params[:id])
  end

  private

  def payment_transaction_params
    params.require(:payment_transaction).permit(:user_id, :amount, :product_id)
  end
end
