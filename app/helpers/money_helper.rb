module MoneyHelper
  def to_cents(amount)
    (amount * 100).round
  end
end