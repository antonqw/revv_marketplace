# README

### About project
* Ruby version 2.5.1

### Get started
1. Clone repo
```
    git clone git@bitbucket.org:antonqw/revv_marketplace.git
    cd revv_marketplace
```
2.Create gemset, if it has not created yet
```console
    rvm use 2.5.1@revv_marketplace --create
```
3. Run bundle
```console
    bundle install
```
4. Create config/master.key file and put here secret key.
```console
   touch config/master.key
```
4. Create database and run migration
```console
    rake db:create
    rake db:migrate
```
5. Run tests
```ruby
    rspec
```
